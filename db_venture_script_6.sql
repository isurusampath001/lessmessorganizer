USE [master]
GO
/****** Object:  Database [db_venture]    Script Date: 5/20/2021 2:26:08 PM ******/
CREATE DATABASE [db_venture]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_venture_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\db_venture.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'db_venture_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\db_venture.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [db_venture] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_venture].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_venture] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_venture] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_venture] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_venture] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_venture] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_venture] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_venture] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_venture] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_venture] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_venture] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_venture] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_venture] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_venture] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_venture] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_venture] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_venture] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_venture] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_venture] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_venture] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_venture] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_venture] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_venture] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_venture] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_venture] SET  MULTI_USER 
GO
ALTER DATABASE [db_venture] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_venture] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_venture] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_venture] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_venture] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_venture] SET QUERY_STORE = OFF
GO
USE [db_venture]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [varchar](20) NOT NULL,
	[category_desc] [varchar](255) NULL,
	[is_active] [int] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Designation]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Designation](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[d_name] [varchar](50) NULL,
	[grade] [varchar](10) NOT NULL,
	[section] [varchar](255) NULL,
	[is_active] [int] NULL,
 CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Follower]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Follower](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[follower_id] [varchar](50) NOT NULL,
	[follower_user_id] [varchar](50) NOT NULL,
	[venture_id] [varchar](50) NOT NULL,
	[added_user_id] [varchar](50) NOT NULL,
	[added_time] [datetime] NULL,
	[follower_status] [varchar](10) NULL,
	[is_active] [int] NULL,
 CONSTRAINT [PK_Follower] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdReference]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdReference](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[ref_value] [int] NOT NULL,
	[ref_description] [varchar](50) NULL,
 CONSTRAINT [PK_IdReference] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[log_id] [varchar](50) NOT NULL,
	[venture_id] [varchar](50) NOT NULL,
	[priority_id] [int] NOT NULL,
	[added_time] [datetime] NULL,
	[user_id] [varchar](50) NOT NULL,
	[description] [varchar](255) NULL,
	[is_active] [int] NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Priority]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Priority](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[priority] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Priority] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Privilege]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Privilege](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[p_level] [int] NULL,
	[privilage_name] [varchar](50) NULL,
 CONSTRAINT [PK_Privilege] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SecurityClassifier]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SecurityClassifier](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[sec_classifier] [varchar](50) NOT NULL,
 CONSTRAINT [PK_SecurityClassfier] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[status_desc] [varchar](255) NOT NULL,
	[status_type_id] [int] NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StatusType]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatusType](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[status_type_desc] [varchar](255) NULL,
 CONSTRAINT [PK_StatusType] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[u_id] [varchar](50) NOT NULL,
	[u_email] [varchar](255) NULL,
	[u_password] [varchar](50) NOT NULL,
	[f_name] [varchar](50) NOT NULL,
	[m_name] [varchar](50) NULL,
	[l_name] [varchar](50) NULL,
	[title] [varchar](10) NULL,
	[designation_id] [int] NOT NULL,
	[contact_no_1] [varchar](15) NOT NULL,
	[contact_no_2] [varchar](15) NULL,
	[contact_no_3] [varchar](15) NULL,
	[entered_by] [varchar](50) NULL,
	[entered_time] [datetime] NULL,
	[status] [varchar](10) NOT NULL,
	[slt_employee_no] [varchar](6) NULL,
	[updated_by] [varchar](50) NULL,
	[updated_time] [datetime] NULL,
	[is_active] [int] NOT NULL,
	[u_privilege] [int] NULL,
	[last_logged_time] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venture]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venture](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[venture_ref] [varchar](100) NULL,
	[p_venture_ref] [varchar](100) NULL,
	[initiate_time] [datetime] NOT NULL,
	[initiate_user_id] [varchar](50) NOT NULL,
	[category_id] [varchar](20) NOT NULL,
	[description] [varchar](255) NULL,
	[status_id] [int] NOT NULL,
	[last_updated_by] [varchar](50) NOT NULL,
	[last_updated_time] [datetime] NOT NULL,
	[security_classification] [int] NULL,
	[identifier] [int] NULL,
	[is_active] [int] NULL,
	[start_date] [datetime] NULL,
	[end_date] [datetime] NULL,
 CONSTRAINT [PK_Venture] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VentureAssign]    Script Date: 5/20/2021 2:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VentureAssign](
	[i_id] [int] IDENTITY(1,1) NOT NULL,
	[venture_ref] [varchar](100) NOT NULL,
	[assign_user_id] [varchar](50) NOT NULL,
	[initiate_user_id] [varchar](50) NULL,
	[updated_user_id] [varchar](50) NULL,
	[updated_time] [datetime] NULL,
 CONSTRAINT [PK_VentureAssign] PRIMARY KEY CLUSTERED 
(
	[i_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([i_id], [category_id], [category_desc], [is_active]) VALUES (1, N'cat01', N'Task', 1)
INSERT [dbo].[Category] ([i_id], [category_id], [category_desc], [is_active]) VALUES (2, N'cat02', N'Project', 1)
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Designation] ON 

INSERT [dbo].[Designation] ([i_id], [d_name], [grade], [section], [is_active]) VALUES (1, N'Software developer', N'A9', N'Digital Platform', 1)
INSERT [dbo].[Designation] ([i_id], [d_name], [grade], [section], [is_active]) VALUES (2, N'IT & Network Officer', N'A9', N'Application Integration Section', 1)
INSERT [dbo].[Designation] ([i_id], [d_name], [grade], [section], [is_active]) VALUES (3, N'Manager', N'A5', N'Software Quality Assurance', 1)
INSERT [dbo].[Designation] ([i_id], [d_name], [grade], [section], [is_active]) VALUES (4, N'Eng-System Development', N'A4', N'Digital Platform', 1)
INSERT [dbo].[Designation] ([i_id], [d_name], [grade], [section], [is_active]) VALUES (5, N'Technician', N'A13', N'', 1)
INSERT [dbo].[Designation] ([i_id], [d_name], [grade], [section], [is_active]) VALUES (6, N'Sales Officer', N'A12', N'', 1)
SET IDENTITY_INSERT [dbo].[Designation] OFF
GO
SET IDENTITY_INSERT [dbo].[Follower] ON 

INSERT [dbo].[Follower] ([i_id], [follower_id], [follower_user_id], [venture_id], [added_user_id], [added_time], [follower_status], [is_active]) VALUES (1, N'f001', N'u0014', N'vref0042', N'u0025', CAST(N'2021-04-30T11:30:00.000' AS DateTime), N'1', 1)
SET IDENTITY_INSERT [dbo].[Follower] OFF
GO
SET IDENTITY_INSERT [dbo].[IdReference] ON 

INSERT [dbo].[IdReference] ([i_id], [ref_value], [ref_description]) VALUES (1, 10053, N'Venture')
INSERT [dbo].[IdReference] ([i_id], [ref_value], [ref_description]) VALUES (2, 20000, N'User')
INSERT [dbo].[IdReference] ([i_id], [ref_value], [ref_description]) VALUES (3, 30000, N'Follower')
SET IDENTITY_INSERT [dbo].[IdReference] OFF
GO
SET IDENTITY_INSERT [dbo].[Priority] ON 

INSERT [dbo].[Priority] ([i_id], [priority]) VALUES (1, N'High')
INSERT [dbo].[Priority] ([i_id], [priority]) VALUES (2, N'Medium')
INSERT [dbo].[Priority] ([i_id], [priority]) VALUES (3, N'Low')
SET IDENTITY_INSERT [dbo].[Priority] OFF
GO
SET IDENTITY_INSERT [dbo].[Privilege] ON 

INSERT [dbo].[Privilege] ([i_id], [p_level], [privilage_name]) VALUES (1, 1, N'Admin')
INSERT [dbo].[Privilege] ([i_id], [p_level], [privilage_name]) VALUES (2, 2, N'Manager')
INSERT [dbo].[Privilege] ([i_id], [p_level], [privilage_name]) VALUES (3, 3, N'Viewer')
SET IDENTITY_INSERT [dbo].[Privilege] OFF
GO
SET IDENTITY_INSERT [dbo].[SecurityClassifier] ON 

INSERT [dbo].[SecurityClassifier] ([i_id], [sec_classifier]) VALUES (5, N'Internal Use Only')
INSERT [dbo].[SecurityClassifier] ([i_id], [sec_classifier]) VALUES (6, N'Public')
INSERT [dbo].[SecurityClassifier] ([i_id], [sec_classifier]) VALUES (7, N'Confidential')
INSERT [dbo].[SecurityClassifier] ([i_id], [sec_classifier]) VALUES (8, N'Strictly Confidential')
SET IDENTITY_INSERT [dbo].[SecurityClassifier] OFF
GO
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (1, N'active', 1)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (2, N'deactive', 1)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (3, N'no pay leave', 1)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (4, N'on leave', 1)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (5, N'suspended', 1)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (6, N'open', 2)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (7, N'closed', 2)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (8, N'cancelled', 2)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (9, N'Pending', 2)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (10, N'following', 3)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (11, N'unfollowing', 3)
INSERT [dbo].[Status] ([i_id], [status_desc], [status_type_id]) VALUES (12, N'hold', 3)
SET IDENTITY_INSERT [dbo].[Status] OFF
GO
SET IDENTITY_INSERT [dbo].[StatusType] ON 

INSERT [dbo].[StatusType] ([i_id], [status_type_desc]) VALUES (1, N'user status')
INSERT [dbo].[StatusType] ([i_id], [status_type_desc]) VALUES (2, N'venture status')
INSERT [dbo].[StatusType] ([i_id], [status_type_desc]) VALUES (3, N'follower status')
SET IDENTITY_INSERT [dbo].[StatusType] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([i_id], [u_id], [u_email], [u_password], [f_name], [m_name], [l_name], [title], [designation_id], [contact_no_1], [contact_no_2], [contact_no_3], [entered_by], [entered_time], [status], [slt_employee_no], [updated_by], [updated_time], [is_active], [u_privilege], [last_logged_time]) VALUES (1, N'u0025', N'isurusampath@slt.com.lk', N'admin123', N'Isuru', N'', N'Sampath', N'Mr', 1, N'0714869750', N'', N'', N'u0001', CAST(N'2021-03-16T09:33:20.000' AS DateTime), N'1', N'015751', N'u0025', CAST(N'2021-04-29T12:30:10.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[User] ([i_id], [u_id], [u_email], [u_password], [f_name], [m_name], [l_name], [title], [designation_id], [contact_no_1], [contact_no_2], [contact_no_3], [entered_by], [entered_time], [status], [slt_employee_no], [updated_by], [updated_time], [is_active], [u_privilege], [last_logged_time]) VALUES (2, N'u0045', N'romaine@slt.com.lk', N'admin12', N'Romain', N'', N'Murcott', N'Miss', 1, N'0702737220', N'', N'', N'u0001', CAST(N'2021-03-22T11:33:20.000' AS DateTime), N'3', N'015777', N'u0045', CAST(N'2021-04-29T16:30:10.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[User] ([i_id], [u_id], [u_email], [u_password], [f_name], [m_name], [l_name], [title], [designation_id], [contact_no_1], [contact_no_2], [contact_no_3], [entered_by], [entered_time], [status], [slt_employee_no], [updated_by], [updated_time], [is_active], [u_privilege], [last_logged_time]) VALUES (3, N'u0014', N'sarawana@slt.com.lk', N'admin1', N'Sarawanaraj', N'', N'S', N'Mr', 1, N'0719988210', N'', N'', N'u0001', CAST(N'2021-03-16T09:23:20.000' AS DateTime), N'4', N'015750', N'u0014', CAST(N'2021-04-29T12:30:10.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[User] ([i_id], [u_id], [u_email], [u_password], [f_name], [m_name], [l_name], [title], [designation_id], [contact_no_1], [contact_no_2], [contact_no_3], [entered_by], [entered_time], [status], [slt_employee_no], [updated_by], [updated_time], [is_active], [u_privilege], [last_logged_time]) VALUES (4, N'u0099', N'kasun@slt.com.lk', N'kasun123', N'Kasun', N'', N'Rajapaksha', N'Mr', 6, N'0712244350', N'', N'', N'u0001', CAST(N'2021-03-17T09:33:20.000' AS DateTime), N'5', N'015821', N'u0099', CAST(N'2021-05-01T12:30:10.000' AS DateTime), 1, 2, CAST(N'2021-05-02T05:30:10.000' AS DateTime))
INSERT [dbo].[User] ([i_id], [u_id], [u_email], [u_password], [f_name], [m_name], [l_name], [title], [designation_id], [contact_no_1], [contact_no_2], [contact_no_3], [entered_by], [entered_time], [status], [slt_employee_no], [updated_by], [updated_time], [is_active], [u_privilege], [last_logged_time]) VALUES (5, N'u0159', N'samadi@slt.com.lk', N'sama1994', N'Samadi', N'', N'neluksha', N'Miss', 5, N'0777234350', N'', N'', N'u0001', CAST(N'2021-04-17T09:33:20.000' AS DateTime), N'5', N'015111', N'u0159', CAST(N'2021-05-02T08:30:10.000' AS DateTime), 1, 3, CAST(N'2021-05-02T05:30:10.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
GO
USE [master]
GO
ALTER DATABASE [db_venture] SET  READ_WRITE 
GO
