﻿$("#email").on('keyup', function () {
    var email = $("#email").val();
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
        $("#validateEmail").html("Invalid Email");
        $("#validateEmail").css('color', 'red');
        sessionStorage.setItem("loginInvalid", true);
        return false;
    } else {
        $("#validateEmail").html("");
        sessionStorage.setItem("loginInvalid", false);
        return true;
    }
})

$("#password").on('keyup', function () {
    var password = $("#password").val();
    if (password.length < 5) {
        $("#validatePassword").html("Password must contain at least 5 characters");
        $("#validatePassword").css('color', 'red');
        sessionStorage.setItem("loginInvalid", true);

    } else {
        $("#validatePassword").html("");
        sessionStorage.setItem("loginInvalid", false);
    }
})