﻿
$(document).ready(function () {
    $("#submitButton").hide();
    $("#settingSubmitButton").hide();
    $("#accountForm").show();
    $("#settingsForm").hide();
    $('#optionButton1').addClass('btn-primary');
    $('#optionButton2').addClass('btn-outline-primary');
    //$('#successAlert').hide();
    //$('#errorAlert').hide();
    $("#wizard-picture").change(function () {
        readURL(this);
    });

});


$("#optionButton1").on('click', function () {
    $("#settingSubmitButton").hide();
    $("#submitButton").hide();
    $("#settingsForm").hide();
    $("#accountForm").show();
    $('#optionButton1').removeClass('btn-outline-primary');
    $('#optionButton1').addClass('btn-primary');
    $('#optionButton2').removeClass('btn-primary');
    $('#optionButton2').addClass('btn-outline-primary');
});

$("#optionButton2").on('click', function () {
    $("#settingSubmitButton").hide();
    $("#submitButton").hide();
    $("#settingsForm").show();
    $("#accountForm").hide();
    $('#optionButton2').removeClass('btn-outline-primary');
    $('#optionButton2').addClass('btn-primary');
    $('#optionButton1').removeClass('btn-primary');
    $('#optionButton1').addClass('btn-outline-primary');
    $("#confirmPass").hide();
});


$("#editPass").on('click', function () {
    $("#confirmPass").show();
    $("#submitButton").show();
});

function edit(id,event) {
 
    var element = $(id);
    event.preventDefault();
    element.removeAttr('readonly');
    $("#submitButton").show();
    $("#settingSubmitButton").show();

}

function editSelect(id, event) {

    var element = $(id);
    event.preventDefault();
    element.removeAttr('disabled');
    $("#submitButton").show();
    $("#settingSubmitButton").show();

}

$("#submitButton").on('click', function (event) {
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: 'saveProfile',
        data: {
            u_email: $("#email").val(),
            u_password: $("#password").val(),
            f_name: $("#fname").val(),
            m_name: $("#mname").val(),
            l_name: $("#lname").val(),
            title: $("#title").val(),
            designation: $("#designation ").val(),
            contact_no_1: $("#contact1").val(),
            contact_no_2: $("#contact2").val(),
            contact_no_3: $("#contact3").val(),
            status: $("#status").val(),
            slt_employee_no: $("#sltEmpNo").val()

        },
        dataType: "json",
        success: function (data) {
            console.log('data----- ', data);

            if (data.data) {
                //$('#successAlert').show();
                //$('#successAlert').on('closed.bs.alert', function () {
                //    location.reload();
                //})
                location.reload();
            }
        },
        error: function (ex) {
            //$('#errorAlert').show();
        }
    });
    
});

$("#settingSubmitButton").on('click', function () {
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: 'saveSettings',
        data: {
            u_email: $("#username").val(),
            u_password: $("#password").val(),
          
        },
        dataType: "json",
        success: function (data) {
           
            if (data.data) {
               
                location.reload();
            }
        },
        error: function (ex) {
           
        }
    });

});

$("#password").on('keyup', function () {
    var password = $("#password").val();
    var str;
    if (password.length < 8) {
        str = "Password must have at least 8 characters";
        $("#passValidator").text(str);
    } else {
        $("#passValidator").text("");
    }
    $("#settingSubmitButton").show();

});

$("#confirm").on('keyup', function () {
    var password = $("#password").val();
    var confirmed = $("#confirm").val();
    var str;
    if (confirmed.length == 0) {
        str = "Please Confirm Password!";
        $("#matchPassword").text(str);
        $("#matchPassword").css("color", "red");
        $("#confirm").css("border-color", "red");

    } else {
        if (password === confirmed) {
            str = "Passwords matched!";
            $("#matchPassword").text(str);
            $("#confirm").css("border-color", "#ccc");
        } else {
            console.log('didnt match');
            str = "Passwords didn't match!";
            $("#matchPassword").text(str);
            $("#matchPassword").css("color", "red");
            $("#confirm").css("border-color", "red");
        }
    }


});



