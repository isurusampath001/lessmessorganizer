﻿
$("#urSubmitButton").on('click', function (event) {
    event.preventDefault();

    $.ajax(
        {
            type: 'post',
            url: 'saveRegisterUser',
            data: {
                u_email: $("#u_email").val(),
                u_password: $("#u_password").val(),
                f_name: $("#f_name").val(),
                m_name: $("#m_name").val(),
                l_name: $("#l_name").val(),
                title: $("#title").val(),
                designation_id: $("#designation_id").val(),
                contact_no_1: $("#contact_no_1").val(),
                contact_no_2: $("#contact_no_2").val(),
                contact_no_3: $("#contact_no_3").val(),
                status: $("#status").val(),
                u_privilege:$("#u_privilege").val(),
                slt_employee_no: $("#slt_employee_no").val()

            },
            datatype: "json",
            success: function (data) {

                if (data.data) {
                    window.location.pathname = "Login/indexLogin";
                }
            },
            error: function (ex) {
               
            }
         }
    );
 
});

$("#u_email").on('keyup', function () {
    var email = $("#u_email").val();
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
        $("#validateEmail").html("Invalid Email");
        $("#validateEmail").css('color', 'red');
        $("#urSubmitButton").prop("disabled", true);
        return false;
    } else {
        $("#validateEmail").html("");
        $("#urSubmitButton").prop("disabled", false);
        return true;
    }
});

$("#u_password").on('keyup', function () {
    var password = $("#u_password").val();
    if (password.length < 5) {
        $("#validatePass").html("Password must contain at least 5 characters");
        $("#validatePass").css('color', 'red');
        $("#urSubmitButton").attr("disabled", true);
    } else {
        $("#validatePass").html("");
        $("#urSubmitButton").attr("disabled", false);
    }
});

$("#confirmPassword").on('keyup', function () {
    var password = $("#u_password").val();
    var confPass = $("#confirmPassword").val();
    if (password !== confPass) {
        $("#validateConfirmPass").html("Passwords don't match!");
        $("#validateConfirmPass").css('color', 'red');
        $("#urSubmitButton").attr("disabled", true);
    } else {
        $("#validateConfirmPass").html("");
        $("#urSubmitButton").attr("disabled", false);
    }
});

$("#backButton").on('click', function (event) {
    event.preventDefault();
    window.location.pathname = "Login/indexLogin";
});
