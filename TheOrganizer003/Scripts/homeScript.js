﻿function tableGen(filter_data, filter_url,filter_uid) {
    var table = $("#tblVentureList").DataTable();
    table.clear().destroy();
    $('#tblVentureList').DataTable(
        {

            "createdRow": function (row, data, dataIndex, cells) {
                if (data['initiate_user_id'] == filter_uid) {
                    $(row).addClass('table-success');
                } else {
                    $(row).addClass('table-warning');
                }
            },
            "responsive": true,
            "lengthChange": true,
            "autoWidth": false,
            "dom": 'Bfrtip',
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            //"columnDefs": [{
            //    targets: 2,
            //    render: $.fn.dataTable.render.ellipsis()
            //}],
            "ajax": {
                "url": filter_url,
                "type": "POST",
                "data": filter_data,
                "datatype": "json"
            }, "columnDefs": [{
                targets: [2],
                render: $.fn.dataTable.render.ellipsis()
            },
            {
                "defaultContent": "-",
                "targets": "_all"
            },
            {
                "targets": [3],
                "render": function (data, type, row, meta) {

                    var strTimestamp = data.slice(6, 19);
                    var intTimestamp = parseInt(strTimestamp);
                    var date = new Date(intTimestamp);
                    var strDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
                    return strDate;
                }
            },
            {
                "targets": [4],
                "render": function (data, type, row, meta) {

                    var strTimestamp = data.slice(6, 19);
                    var intTimestamp = parseInt(strTimestamp);
                    var date = new Date(intTimestamp);
                    var strDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
                    return strDate;
                }
            },
            {
                "targets": [9],
                "render": function (data, type, row, meta) {

                    var strTimestamp = data.slice(6, 19);
                    var intTimestamp = parseInt(strTimestamp);
                    var date = new Date(intTimestamp);
                    var strDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + "</br>"
                        + date.getHours() + " :" + date.getMinutes() + " :" + date.getSeconds();
                    return strDate;
                }
            }
            ],
            "columns": [
                { "data": "venture_ref" },
                { "data": "p_venture_ref" },
                { "data": "description" },
                { "data": "start_date"/*new Date(parseInt("start_date".replace(/\/Date\((.*?)\)\//gi, "$1")))*/ },
                { "data": "end_date" },
                { "data": "status_desc" },
                { "data": "security_classfication" },
                { "data": "initiate_user_id" },
                { "data": "category_desc" },
                { "data": "last_updated_time" },
                {
                    "targets": 11,
                    "data": 'venture_ref',
                    "render": function (data, type, row, meta) {
                        return '<button type="button" class="btn-xs btn-primary tbox" id="buttonSelectVenture" value ="' + data + '" ><i class="fas fa-edit" ></i> Select</button>'
                    }
                }
            ]
        }
    )
}