//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheOrganizer003.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class VentureAssign
    {
        public int i_id { get; set; }
        public string venture_ref { get; set; }
        public string assign_user_id { get; set; }
        public string initiate_user_id { get; set; }
        public string updated_user_id { get; set; }
        public Nullable<System.DateTime> updated_time { get; set; }
    }
}
