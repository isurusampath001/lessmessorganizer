﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheOrganizer003.Models
{
    public class MyModel
    {
        public Venture Venture1 { get; set; }
        public Category Category1 { get; set; }
        public Status Status1 { get; set; }
        public VentureAssign VentureAssign1 { get; set; }
        public SecurityClassifier SecurityClassifier1 { get; set; }
    }
}