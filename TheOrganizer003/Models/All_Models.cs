﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheOrganizer003.Models
{
    public class All_Models
    {
        public Venture Venture1 { get; set; }
        public Venture Venture2 { get; set; }
        public IEnumerable<MyModel> myModels { get; set; }
    }
}