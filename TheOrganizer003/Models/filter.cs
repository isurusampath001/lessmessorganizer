﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheOrganizer003.Models
{
    public class filter
    {
        public int i_id { get; set; }
        public string v_id { get; set; }
        public string venture_ref { get; set; }
        public string p_venture_ref { get; set; }
        public System.DateTime initiate_time { get; set; }
        public string initiate_user_id { get; set; }
        public string category_id { get; set; }
        public string description { get; set; }
        public int status_id { get; set; }
        public string last_updated_by { get; set; }
        public System.DateTime last_updated_time { get; set; }
        public string security_classfication { get; set; }
        public string identifier { get; set; }
        public Nullable<int> is_active { get; set; }
        public Nullable<System.DateTime> start_date { get; set; }
        public Nullable<System.DateTime> end_date { get; set; }
        public string category_desc { get; set; }
        public string status_desc { get; set; }
    }
}