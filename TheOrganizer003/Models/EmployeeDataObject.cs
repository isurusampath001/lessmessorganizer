﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheOrganizer003.Models
{
    public class EmployeeDataObject
    {
        public string serviceNumber { get; set; }
        public string employeeInitials { get; set; }
        public string employeeFirstName { get; set; }
        public string employeeLastName { get; set; }
        public string employeeOfficialEmail { get; set; }
        public string employeeNIC { get; set; }
        public string employeeOfficialMobile { get; set; }
        public string employeeSection { get; set; }
        public string employeeDivision { get; set; }
        public string employeeGroup { get; set; }
        public string employeeCostCenter { get; set; }
        public string employeeImmediateSupNumber { get; set; }
        public string employeeTitle { get; set; }
    }
}