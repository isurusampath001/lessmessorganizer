﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheOrganizer003.Models;
using System.Data;
using Newtonsoft.Json;
using System.Collections;
using TheOrganizer003.Util;

namespace TheOraganizer003.Controllers
{
    [SessionCheck]
    public class HomeController : Controller
    {
       

        db_ventureEntities context = new db_ventureEntities();


        public ActionResult IndexHome()
        {
            string uid = Session["uid"].ToString();
            
            //log
            (new Utils()).addALog("", 3, (string)Session["uid"], "view home ");

            var getVenture = (from v in context.Ventures.AsEnumerable()
                        join c in context.Categories.AsEnumerable()
                        on v.category_id equals c.category_id
                        join s in context.Status.AsEnumerable()
                        on v.status_id equals s.i_id
                        join v_a in context.VentureAssigns.AsEnumerable()
                        on v.venture_ref equals v_a.venture_ref
                        join s_c in context.SecurityClassifiers.AsEnumerable()
                        on v.security_classification equals s_c.i_id
                        where (v.initiate_user_id == uid || v_a.assign_user_id == uid ) && v.is_active == 1
                        select new MyModel
                        {
                            Venture1 = v,
                            VentureAssign1 = v_a,
                            Status1 = s,
                            Category1 = c,
                            SecurityClassifier1 = s_c

                      });
            var model = new All_Models();
            model.myModels = getVenture;
            var cur_dt = DateTime.Now.AddDays(7);
            var notification = getVenture.Where( MyModel => MyModel.Venture1.end_date <= cur_dt);
            var notification_count = notification.Count();
            int ventureCount = getVenture.Count();
            
            var getCategory = context.Categories.SqlQuery("select * from Category");
            var getStatus = context.Status.SqlQuery("select * from Status where status_type_id = 2");
            var getSecurityClassifier = context.SecurityClassifiers.SqlQuery("select * from SecurityClassifier");
            var venCount = context.Ventures.SqlQuery("" +
                "select count(venture_ref) from Venture as v inner join VentureAssign as va " +
                "on v.venture_ref = va.venture_ref where va.assign_user_id = '" + uid + "' or va.initiate_user_id = '" + uid + "'; ");

            Session["ven_count"] = ventureCount;//venCount;

            int emp_no = Convert.ToInt32(Session["emp_no"]);

            List<SelectListItem> status = new List<SelectListItem>();
            List<SelectListItem> list = new List<SelectListItem>();
            List<SelectListItem> s_classifier = new List<SelectListItem>();
            status.Add(new SelectListItem()
            {
                Text = "Select".ToString(),
                Value = "0".ToString()
            });
            foreach (var row in getStatus)
            {
                status.Add(new SelectListItem()
                {
                    Text = row.status_desc.ToString(),
                    Value = row.i_id.ToString()
                });
            }
            foreach (var row in getCategory)
            {
                list.Add(new SelectListItem()
                {
                    Text = row.category_desc.ToString(),
                    Value = row.category_id.ToString()
                });
            }
            foreach (var row in getSecurityClassifier)
            {
                s_classifier.Add(new SelectListItem()
                {
                    Text = row.sec_classifier.ToString(),
                    Value = row.i_id.ToString()
                });
            }

            
            
            ViewData["Categories"] = new SelectList(list, "Value", "Text");
            ViewData["Status"] = new SelectList(status, "Value", "Text");
            ViewData["s_classifier"] = new SelectList(s_classifier, "Value", "Text");
            ViewData["notification"] = notification;
            ViewData["notification_count"] = notification_count;
            return View(model);
        }

        public ActionResult GetUserMain()
        {
            string uid = Session["uid"].ToString();
            var loggedUser = context.Users.SqlQuery("select * from [User] where u_id = '" + uid + "'");
            return Json(loggedUser, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateVenture(All_Models data)
        {
            string uid = Session["uid"].ToString();
            //Test2Controller Change
            var code = context.IdReferences.SqlQuery("Select * from IdReference where i_id = 1").FirstOrDefault<IdReference>();
            var newCode = (code.ref_value + 1);
            using (var db = new db_ventureEntities())
            {
                var result = db.IdReferences.SingleOrDefault(b => b.i_id == 1);
                if (result != null)
                {
                    result.ref_value = newCode;
                    db.SaveChanges();
                }
            }
            
            var cur_dt = DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss");
            string start_date = data.Venture1.start_date.Value.ToString("MM-dd-yyyy");
            string end_date = data.Venture1.end_date.Value.ToString("MM-dd-yyyy");
            int row_ins = context.Database.ExecuteSqlCommand("Insert into Venture values ('" +
                    "V" + code.ref_value + "','" +
                    " " + "','" +
                    cur_dt + "','" +
                    uid + "','" +
                    data.Venture1.category_id + "','" +
                    data.Venture1.description + "','" +
                    data.Venture1.status_id + "','" +
                    uid + "','" +
                    cur_dt + "','" +
                    data.Venture1.security_classification + "','" +
                    data.Venture1.identifier + "','" +
                    1 + "','" +
                    start_date + "','" +
                    end_date +
                     "')");


            int row_ins_2 = context.Database.ExecuteSqlCommand("Insert into VentureAssign values ('" +
                    "V" + code.ref_value + "','"+
                    uid + "','" +
                    uid + "','" +
                    uid + "','" +
                    cur_dt +
                     "')");

            //log
            (new Utils()).addALog("V" + code.ref_value, 2, (string)Session["uid"], "create venture-ref : " + "V" + code.ref_value);

            //must check if row inserted or not
            //update idreference table
            //(new TheOrganizer003.Util.Utils()).UpdateIdReference(1, emp_no, incrementNo);

            //if (row_ins > 0)
            //{
            //   Session["last_record"] = "V" + code.ref_value;
            //}
            //else
            //{
            //    Session["last_record"] = "Error";
            //}
            return RedirectToRoute("dashboard");
        }

        //FIltering Test
        public ActionResult getVentur(string x,string y)
        {

            var newTableData = context.Ventures.SqlQuery("" +
                "select * from Venture as v inner join VentureAssign as va on v.venture_ref = va.venture_ref " +
                "where va.initiate_user_id = '" + x + "' OR va.assign_user_id ='" + y + "'");
            return Json(new { data = newTableData }, JsonRequestBehavior.AllowGet);
        }


        //FIltering Based on Start Date 
        public ActionResult filterTable_stdate(string filter1, string filter2)
        {
            String uid = Session["uid"].ToString();
            if (filter1 != null && filter2 != null)
            {
                //string fil = filter1;
                //string start_date_1 = filter1.Substring(5, 2)+"-"+filter1.Substring(8,2)+"-"+filter1.Substring(0,4);
                //string start_date_2 = filter2.Substring(5, 2) + "-" + filter2.Substring(8, 2) + "-" + filter2.Substring(0, 4);
                string new_start_date_1 = filter1.Substring(5, 2) + "-" + filter1.Substring(8, 2) + "-" + filter1.Substring(0, 4);
                string new_start_date_2 = filter2.Substring(5, 2) + "-" + filter2.Substring(8, 2) + "-" + filter2.Substring(0, 4);

                var x = DateTime.Parse(filter1).Date;
                var y = DateTime.Parse(filter2).Date;
                var newTableData1 = context.Ventures.SqlQuery("select * from Venture where start_date>='" + new_start_date_1 + "' AND start_date<='" + new_start_date_2 + "' AND initiate_user_id = '" + Session["uid"] + "'");

                var newTableData = (from v in context.Ventures.AsEnumerable()
                                  join c in context.Categories.AsEnumerable()
                                  on v.category_id equals c.category_id
                                  join s in context.Status.AsEnumerable()
                                  on v.status_id equals s.i_id
                                  join v_a in context.VentureAssigns.AsEnumerable()
                                  on v.venture_ref equals v_a.venture_ref
                                  join s_c in context.SecurityClassifiers.AsEnumerable()
                                  on v.security_classification equals s_c.i_id
                                  where (v.initiate_user_id == uid || v_a.assign_user_id == uid) && v.is_active == 1 && v.start_date >= x && v.start_date <= y
                                  select new 
                                  {
                                      Venture = v,
                                      VentureAssign = v_a,
                                      Category = c,
                                      Status = s,
                                      SecurityClassifier = s_c
                                  }).ToList();
                //return Json(new { data = newTableData }, JsonRequestBehavior.AllowGet);
                //List<Venture> list = new List<Venture>();
                //List<Category> list1 = new List<Category>();
                //List<Status> list2 = new List<Status>();
                List<filter> list = new List<filter>();
                foreach (var row in newTableData)
                {
                    //    //    //DateTime isoJson = DateTime.Parse(JsonConvert.SerializeObject(row.start_date.Value.ToShortDateString()));
                    list.Add(new filter()
                    {
                        venture_ref = row.Venture.venture_ref.ToString(),
                        p_venture_ref = row.Venture.p_venture_ref.ToString(),
                        description = row.Venture.description.ToString(),
                        start_date = row.Venture.start_date,
                        end_date = row.Venture.end_date,
                        status_id = row.Venture.status_id,
                        security_classfication = row.SecurityClassifier.sec_classifier,
                        initiate_user_id = row.Venture.initiate_user_id,
                        category_id = row.Venture.category_id,
                        last_updated_time = row.Venture.last_updated_time,
                        status_desc = row.Status.status_desc,
                        category_desc = row.Category.category_desc
                    });
                    //list1.Add(new Category() {
                    //    category_desc = row.Category.category_desc
                    //});
                    //list2.Add(new Status() {
                    //    status_desc = row.Status.status_desc
                    //});
                    
                }

                return Json(new { data = list}, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return null;
            }
        }

        //FIltering Based on End Date 
        public ActionResult filterTable_edate(string filter1, string filter2)
        {
            String uid = Session["uid"].ToString();
            if (filter1 != null && filter2 != null)
            {
                var x = DateTime.Parse(filter1).Date;
                var y = DateTime.Parse(filter2).Date;
                
                var newTableData = (from v in context.Ventures.AsEnumerable()
                                    join c in context.Categories.AsEnumerable()
                                    on v.category_id equals c.category_id
                                    join s in context.Status.AsEnumerable()
                                    on v.status_id equals s.i_id
                                    join v_a in context.VentureAssigns.AsEnumerable()
                                    on v.venture_ref equals v_a.venture_ref
                                    join s_c in context.SecurityClassifiers.AsEnumerable()
                                    on v.security_classification equals s_c.i_id
                                    where (v.initiate_user_id == uid || v_a.assign_user_id == uid) && v.is_active == 1 && v.end_date >= x && v.end_date <= y
                                    select new
                                    {
                                        Venture = v,
                                        VentureAssign = v_a,
                                        Category = c,
                                        Status = s,
                                        SecurityClassifier = s_c
                                    }).ToList();
                //return Json(new { data = newTableData }, JsonRequestBehavior.AllowGet);
                //List<Venture> list3 = new List<Venture>();
                //List<Category> list1 = new List<Category>();
                //List<Status> list2 = new List<Status>();
                List<filter> list = new List<filter>();
                foreach (var row in newTableData)
                {
                    //    //    //DateTime isoJson = DateTime.Parse(JsonConvert.SerializeObject(row.start_date.Value.ToShortDateString()));
                    list.Add(new filter()
                    {
                        venture_ref = row.Venture.venture_ref.ToString(),
                        p_venture_ref = row.Venture.p_venture_ref.ToString(),
                        description = row.Venture.description.ToString(),
                        start_date = row.Venture.start_date,
                        end_date = row.Venture.end_date,
                        status_id = row.Venture.status_id,
                        security_classfication = row.SecurityClassifier.sec_classifier,
                        initiate_user_id = row.Venture.initiate_user_id,
                        category_id = row.Venture.category_id,
                        last_updated_time = row.Venture.last_updated_time,
                        status_desc = row.Status.status_desc,
                        category_desc = row.Category.category_desc
                    });
                    //list1.Add(new Category()
                    //{
                    //    category_desc = row.Category.category_desc
                    //});
                    //list2.Add(new Status()
                    //{
                    //    status_desc = row.Status.status_desc
                    //});

                }

                return Json(new { data = list}, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return null;
            }
        }

        //FIltering Based on Status 
        public ActionResult filterTable_status(string filter1)
        {
            String uid = Session["uid"].ToString();
            if (filter1 != "0")
            {
                int filt = int.Parse(filter1);

                var newTableData = (from v in context.Ventures.AsEnumerable()
                                    join c in context.Categories.AsEnumerable()
                                    on v.category_id equals c.category_id
                                    join s in context.Status.AsEnumerable()
                                    on v.status_id equals s.i_id
                                    join v_a in context.VentureAssigns.AsEnumerable()
                                    on v.venture_ref equals v_a.venture_ref
                                    join s_c in context.SecurityClassifiers.AsEnumerable()
                                    on v.security_classification equals s_c.i_id
                                    where (v.initiate_user_id == uid || v_a.assign_user_id == uid) && v.is_active == 1 && v.status_id == filt
                                    select new
                                    {
                                        Venture = v,
                                        VentureAssign = v_a,
                                        Category = c,
                                        Status = s,
                                        SecurityClassifier = s_c
                                    }).ToList();
                //return Json(new { data = newTableData }, JsonRequestBehavior.AllowGet);
                //List<Venture> list3 = new List<Venture>();
                //List<Category> list1 = new List<Category>();
                //List<Status> list2 = new List<Status>();
                List<filter> list = new List<filter>();
                foreach (var row in newTableData)
                {
                    //    //    //DateTime isoJson = DateTime.Parse(JsonConvert.SerializeObject(row.start_date.Value.ToShortDateString()));
                    list.Add(new filter()
                    {
                        venture_ref = row.Venture.venture_ref.ToString(),
                        p_venture_ref = row.Venture.p_venture_ref.ToString(),
                        description = row.Venture.description.ToString(),
                        start_date = row.Venture.start_date,
                        end_date = row.Venture.end_date,
                        status_id = row.Venture.status_id,
                        security_classfication = row.SecurityClassifier.sec_classifier,
                        initiate_user_id = row.Venture.initiate_user_id,
                        category_id = row.Venture.category_id,
                        last_updated_time = row.Venture.last_updated_time,
                        status_desc = row.Status.status_desc,
                        category_desc = row.Category.category_desc

                    });
                    //list1.Add(new Category()
                    //{
                    //    category_desc = row.Category.category_desc
                    //});
                    //list2.Add(new Status()
                    //{
                    //    status_desc = row.Status.status_desc
                    //});

                }

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return null;
            }
        }


        public ActionResult filterTable_SecurityClassifier(string filter1)
        {
            String uid = Session["uid"].ToString();
            if (filter1 != "0")
            {
                int filt = int.Parse(filter1);

                var newTableData = (from v in context.Ventures.AsEnumerable()
                                    join c in context.Categories.AsEnumerable()
                                    on v.category_id equals c.category_id
                                    join s in context.Status.AsEnumerable()
                                    on v.status_id equals s.i_id
                                    join v_a in context.VentureAssigns.AsEnumerable()
                                    on v.venture_ref equals v_a.venture_ref
                                    join s_c in context.SecurityClassifiers.AsEnumerable()
                                    on v.security_classification equals s_c.i_id
                                    where (v.initiate_user_id == uid || v_a.assign_user_id == uid) && v.is_active == 1 && v.security_classification == filt
                                    select new
                                    {
                                        Venture = v,
                                        VentureAssign = v_a,
                                        Category = c,
                                        Status = s,
                                        SecurityClassifier = s_c
                                    }).ToList();
                //return Json(new { data = newTableData }, JsonRequestBehavior.AllowGet);
                //List<Venture> list3 = new List<Venture>();
                //List<Category> list1 = new List<Category>();
                //List<Status> list2 = new List<Status>();
                List<filter> list = new List<filter>();
                foreach (var row in newTableData)
                {
                    //    //    //DateTime isoJson = DateTime.Parse(JsonConvert.SerializeObject(row.start_date.Value.ToShortDateString()));
                    list.Add(new filter()
                    {

                        venture_ref = row.Venture.venture_ref.ToString(),
                        p_venture_ref = row.Venture.p_venture_ref.ToString(),
                        description = row.Venture.description.ToString(),
                        start_date = row.Venture.start_date,
                        end_date = row.Venture.end_date,
                        status_id = row.Venture.status_id,
                        security_classfication = row.SecurityClassifier.sec_classifier,
                        initiate_user_id = row.Venture.initiate_user_id,
                        category_id = row.Venture.category_id,
                        last_updated_time = row.Venture.last_updated_time,
                        status_desc = row.Status.status_desc,
                        category_desc = row.Category.category_desc

                    });
                    //list1.Add(new Category()
                    //{
                    //    category_desc = row.Category.category_desc
                    //});
                    //list2.Add(new Status()
                    //{
                    //    status_desc = row.Status.status_desc
                    //});

                }

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return null;
            }
        }

        public ActionResult filterTable2(string status)
        {
            if (status != "0")
            {
                var newTableData = context.Ventures.SqlQuery("select * from Venture where status_id =" + status + " AND initiate_user_id = '" + Session["u_id"] + "'");

                List<Venture> list = new List<Venture>();
                foreach (var row in newTableData)
                {
                    list.Add(new Venture()
                    {
                        venture_ref = row.venture_ref.ToString(),
                        p_venture_ref = row.p_venture_ref.ToString(),
                        description = row.description.ToString(),
                        start_date = row.start_date,
                        end_date = row.end_date,
                        status_id = row.status_id,
                        initiate_user_id = row.initiate_user_id,
                        category_id = row.category_id,
                        last_updated_time = row.last_updated_time

                    });
                }
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return null;
            }
        }
    }


}