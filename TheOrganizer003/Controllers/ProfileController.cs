﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheOrganizer003.Models;
using TheOrganizer003.Util;

namespace TheOraganizer003.Controllers
{
    [SessionCheck]
    public class ProfileController : Controller
    {
        // GET: Profile
        db_ventureEntities context = new db_ventureEntities();
        public ActionResult IndexProfile()
        {
            //Get the data for the user

            if (Session["emp_no"] != null)
            {

                //log
                (new Utils()).addALog("", 3,(string)Session["uid"] ,"view profile ");

                var email = Session["email"].ToString();
            User std = new User();
            
               std = context.Users.Where(x => x.u_email == email).FirstOrDefault<User>();
              // std = context.Users.FirstOrDefault();
                var designationList = context.Designations.SqlQuery("select i_id as designationId,designation1 as designationDesc from Designation where is_active=1");
            //List<Designation> designationList = context.Designations.SqlQuery("select * from Designation where is_active=1").ToList<Designation>();
               ViewData["DesignationList"] = designationList;
               ViewData["userData"] = std;
               ViewData["fullName"] = std.f_name+" "+std.m_name+" "+std.l_name;
               

            return View();
            }
            else
            {
                return RedirectToRoute("Login","indexLogin");
            }
        }

        [HttpPost]
        public JsonResult saveProfile(User user)
        {
            String queryString = "update [User]  set u_email='" + user.u_email+ "',u_password='"+user.u_password+"',f_name='"+user.f_name+"',m_name='"+user.m_name+"',l_name='"+user.l_name+"',title='"+user.title+"',designation_id='"+user.designation_id+"',contact_no_1='"+user.contact_no_1+"',contact_no_2='"+user.contact_no_2+"',contact_no_3='"+user.contact_no_3+"',status='"+user.status+"',slt_employee_no='"+user.slt_employee_no+"' where i_id=" + Session["userId"] + " and is_active=1";
            Trace.WriteLine(queryString);
            User std = new User();
           
            int noOfRowUpdated = context.Database.ExecuteSqlCommand(queryString);
            Trace.WriteLine(noOfRowUpdated);
            if (noOfRowUpdated > 0)
            {
                return Json(new { data = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = false }, JsonRequestBehavior.AllowGet);
            }
 
            
        }

        [HttpPost]
        public JsonResult saveSettings(User user)
        {
            String queryString = "update [User]  set u_email='" + user.u_email + "',u_password='" + user.u_password + "' where i_id=" + Session["userId"] + " and is_active=1";
            Trace.WriteLine(queryString);
            User std = new User();

            int noOfRowUpdated = context.Database.ExecuteSqlCommand(queryString);
            Trace.WriteLine(noOfRowUpdated);
            if (noOfRowUpdated > 0)
            {
                return Json(new { data = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = false }, JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult IndexRegister()
        {
            var privileges = context.Privileges.SqlQuery("select * from Privilege");
            var designations = context.Designations.SqlQuery("select * from Designation");
            //var allStatus = context.Status.SqlQuery("select s.i_id as statusId, s.status_desc as statusDesc from Status s inner join StatusType st on s.status_type_id=st.i_id where st.i_id=1");
            //var selectedVenture = context.Ventures.SqlQuery("select * from Venture where venture_ref = 'vref0022'");
            var allStatus = (from s in context.Status
                          join st in context.StatusTypes
                          on s.status_type_id equals st.i_id
                          where st.i_id == 1
                          select new
                          {
                              statusId = s.i_id,
                              statusDesc = s.status_desc
                          }).ToList();

            var dict = new Dictionary<int, String>();
           // dict["key_name"] = "value1";
            foreach (var x in allStatus)
            {
                dict[x.statusId] = x.statusDesc;
            }
           

            ViewData["priv"] = privileges;
            ViewData["allStatus"] = dict;
            ViewData["designations"] = designations;
            Session["uprivilege"] = 0;
            return View();
           
        }

        public JsonResult SaveRegisterUser(User user)
        {
            String entered = "u0000";
            Trace.WriteLine(user.u_email);
            String query = "insert into [User](u_id,u_email,u_password,f_name,m_name,l_name,title,designation_id,contact_no_1,contact_no_2,contact_no_3,status,slt_employee_no,is_active,u_privilege,entered_by) values('u1000','"+user.u_email+"','"+user.u_password+"','"+user.f_name+"','"+user.m_name+"','"+user.l_name+"','"+user.title+"','"+user.designation_id+"','"+user.contact_no_1+"','"+user.contact_no_2+"','"+user.contact_no_3+"','"+user.status+"',"+user.slt_employee_no+","+1+","+user.u_privilege+",'"+entered+"')";
            Trace.WriteLine(query);
            int noOfRowInserted = context.Database.ExecuteSqlCommand(query);
            Trace.WriteLine(noOfRowInserted);
            if (noOfRowInserted > 0)
            {
                return Json(new { data = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = false }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}