﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TheOrganizer003.Models;
using TheOrganizer003.Util;

namespace TheOraganizer003.Controllers
{
    [SessionCheck]
    public class VentureController : Controller
    {

        db_ventureEntities context = new db_ventureEntities();
        private static string SelectedVenture;

        public ActionResult IndexVen(string vref)
        {

            SelectedVenture = vref;
            var selectedVenture = context.Ventures.SqlQuery("select * from Venture where venture_ref = '" + vref + "'");
            var getStatus = context.Status.SqlQuery("select * from Status where status_type_id = 2");
            var getCategory = context.Categories.SqlQuery("select * from Category");
            var getSecClassifications = context.SecurityClassifiers.SqlQuery("select * from SecurityClassifier");

            //log
            (new Utils()).addALog(SelectedVenture, 3,(string)Session["uid"],"view venture-ref : "+ SelectedVenture);
            
            List<SelectListItem> statusList = new List<SelectListItem>();
            List<SelectListItem> categoryList = new List<SelectListItem>();
            List<SelectListItem> secClassificationsList = new List<SelectListItem>();
            
            statusList.Add(new SelectListItem()
            {
                Text = "Select".ToString(),
                Value = "0".ToString()
            });
            foreach (var row in getStatus)
            {
                statusList.Add(new SelectListItem()
                {
                    Text = row.status_desc.ToString(),
                    Value = row.i_id.ToString()
                });
            }
            foreach (var row in getCategory)
            {
                categoryList.Add(new SelectListItem()
                {
                    Text = row.category_desc.ToString(),
                    Value = row.category_id.ToString()
                });
            }
            foreach (var row in getSecClassifications)
            {
                secClassificationsList.Add(new SelectListItem()
                {
                    Text = row.sec_classifier.ToString(),
                    Value = row.i_id.ToString()
                });
            }

            ViewData["Ventures"] = selectedVenture;
            ViewData["Status"] = new SelectList(statusList, "Value", "Text");
            ViewData["Categories"] = new SelectList(categoryList, "Value", "Text");
            ViewData["secClassification"] = new SelectList(secClassificationsList, "Value", "Text");

            return View();
        }


        //[HttpPost]
        public ActionResult SelectedVen(string id)
        {
            var selectedVenture = context.Ventures.SqlQuery("select * from Venture where venture_ref = '" + id + "'");
            return Json(selectedVenture, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getLogFiles(string ventureRef)
        {
            var selectedLogs = context.Logs.SqlQuery("select * from Log where venture_id = '" + ventureRef + "'");
            return Json(new { data = selectedLogs }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetChildVentures(string pVentureRef)
        {

            string sql = "";
            if (SelectedVenture != null && SelectedVenture.Equals(""))
            {
                sql = "select * from Venture where p_venture_ref = '" + SelectedVenture + "' && p_venture_ref != '' ";
            }
            else
            {
                sql = "select * from Venture where p_venture_ref = '" + SelectedVenture + "'";

            }

            var selectedPVenture = context.Ventures.SqlQuery(sql);
            return Json(new { data = selectedPVenture }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUserListForAssign(string email, string emp_no)
        {
            string sql = "";
            if (email != null && !email.Equals(""))
            {
                sql = "select * from [User] where u_email like '%" + email + "%' ";
            }
            else
            {
                sql = "select * from [User]";
            }
            var filteredUsers = context.Users.SqlQuery(sql);
            return Json(new { data = filteredUsers }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFollowers(string venId)
        {
            var followers = context.Followers.SqlQuery("select * from Follower where venture_id = '" + venId + "'");
            return Json(new { data = followers }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateVenture(Venture vent)
        {
            var current_date = DateTime.Now.ToString();
            
            context.Database.ExecuteSqlCommand("" +
                "UPDATE Venture SET " +
                "venture_ref = '" + vent.venture_ref + "'," +
                "p_venture_ref = '" + vent.p_venture_ref + "'," +
                "initiate_time = '" + vent.initiate_time + "'," +
                "initiate_user_id = '" + vent.initiate_user_id + "'," +
                "category_id = '" + vent.category_id + "'," +
                "description = '" + vent.description + "'," +
                "status_id = '" + vent.status_id + "'," +
                "last_updated_by = '" + Session["uid"] + "'," +
                "last_updated_time = '" + current_date + "'," +
                "security_classfication = '" + vent.security_classification + "'," +
                "identifier = '" + vent.identifier + "'," +
                "is_active = 1 ," +
                "start_date = '" + vent.start_date + "'," +
                "end_date = '" + vent.end_date + "' " +
                "WHERE venture_ref = '" + vent.venture_ref + "'");

            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult CreateChildVenture(Venture vent)
        {
            var v_ref = context.IdReferences.SqlQuery("Select * from IdReference where i_id = 1").FirstOrDefault<IdReference>();
            var newVref = 0;
            if (v_ref != null)
            {
                newVref = (v_ref.ref_value + 1);
                using (var db = new db_ventureEntities())
                {
                    var result = db.IdReferences.SingleOrDefault(b => b.i_id == 1);
                    if (result != null)
                    {
                        result.ref_value = newVref;
                        db.SaveChanges();
                    }
                }
            }

            var cur_dt = DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss");
            string start_date = vent.start_date.Value.ToString("MM-dd-yyyy");
            string end_date = vent.end_date.Value.ToString("MM-dd-yyyy");
            int row_ins = context.Database.ExecuteSqlCommand("Insert into Venture values ('" +
                    "V" +newVref + "','" +
                    vent.p_venture_ref + "','" +
                    cur_dt + "','" +
                    Session["uid"] + "','" +
                    vent.category_id + "','" +
                    vent.description + "','" +
                    vent.status_id + "','" +
                    Session["uid"] + "','" +
                    cur_dt + "','" +
                    vent.security_classification + "','" +
                    vent.identifier + "','" +
                    1 + "','" +
                    start_date + "','" +
                    end_date +
                     "')");

            int row_ins_2 = context.Database.ExecuteSqlCommand("Insert into VentureAssign values ('" +
                    "V"+newVref + "','" +
                    Session["uid"] + "','" +
                    Session["uid"] + "','" +
                    Session["uid"] + "','" +
                    cur_dt +
                     "')");
            
            //log
            (new Utils()).addALog(vent.p_venture_ref, 2, (string)Session["uid"], "create child venture-ref : " + "V" + newVref);


            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult CreateNewUser(string email, string empNo, string privilege)
        {
            //create new user
            //assign to venture
            /*var userRef = context.IdReferences.SqlQuery("Select * from IdReference where ref_id = 2").FirstOrDefault<IdReference>();
            var newUserId = 0;
            if (userRef != null)
            {
                newUserId = (userRef.ref_value + 1);
                using (var db = new db_ventureEntities())
                {
                    var result = db.IdReferences.SingleOrDefault(b => b.ref_id == 1);
                    if (result != null)
                    {
                        result.ref_value = newUserId;
                        db.SaveChanges();
                    }
                }
            }*/
            var user = context.Database.ExecuteSqlCommand("");
            string msg = "Currently Not working";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult UserAssignToAVenture(VentureAssign ventureAssign)
        {
            int employeeNo = 0;

            if (ventureAssign != null)
            {
                string employeeEmail = ventureAssign.assign_user_id;
                employeeNo = int.Parse(ventureAssign.updated_user_id);

                var user = context.Users.SqlQuery("select * from[User] where u_email = '" + employeeEmail + "' or slt_employee_no = " + employeeNo + "");
                string assignedUID = "";
                var uid = "";
                var cur_dt = DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss");
                foreach (var u in user)
                {
                    assignedUID = u.u_id;
                    uid = Convert.ToString(Session["uid"]);
                }
                string sqlUpdate = "update VentureAssign set " +
                    "assign_user_id = '" + assignedUID + "',updated_user_id = '" + uid + "',updated_time = '" + cur_dt + "' " +
                    "where venture_ref = '"+ventureAssign.venture_ref+" ';";
                //string sql = "insert into VentureAssign values ('" + ventureAssign.venture_id + "','" + assignedUID + "','" + ventureAssign.initiate_user_id + "','" + uid + "','" + cur_dt + "');";

                var userAssign = context.Database.ExecuteSqlCommand(sqlUpdate);
                
                //log
                (new Utils()).addALog(ventureAssign.venture_ref, 2, (string)Session["uid"],
                    "assign venture-ref : " + ventureAssign.venture_ref+" to user :"+ assignedUID);
            }

            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult CreateFollower(Follower follower)
        {

            var v_ref = context.IdReferences.SqlQuery("Select * from IdReference where i_id = 3").FirstOrDefault<IdReference>();
            var newVref = 0;
            if (v_ref != null)
            {
                newVref = (v_ref.ref_value + 1);
                using (var db = new db_ventureEntities())
                {
                    var result = db.IdReferences.SingleOrDefault(b => b.i_id == 1);
                    if (result != null)
                    {
                        result.ref_value = newVref;
                        db.SaveChanges();
                    }
                }
            }

            context.Followers.Add(follower);
            context.SaveChanges();
            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        public JsonResult GetVentures()
        {
            List<Venture> items = new List<Venture>();
            items = context.Ventures.ToList();
            return Json(items, JsonRequestBehavior.AllowGet);
        }

    }
}