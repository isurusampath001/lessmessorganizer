﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheOrganizer003.Models;

namespace TheOrganizer003.Controllers
{
    public class LoginController : Controller
    {
        private const string ViewName = "./";

        // GET: Login
        db_ventureEntities context = new db_ventureEntities();
        public ActionResult IndexLogin()
        {
            Session.Clear();
            ViewData.Clear();
            return View();
        }

        [HttpPost]
        public ActionResult DoLogin(Models.User u)
        {
            Trace.WriteLine("login valid" + Session["loginInvalid"]);
            if (ModelState.IsValid)
            {
                var str1 = "";
                var str2 = "";
                if (u.u_email == null)
                {
                    str1 = "Please Enter the Username";
                    TempData["vaidation"] = str1;

                    if (u.u_password == null)
                    {
                        str2 = "Please Enter the Password";
                        TempData["vaidation"] = str2;
                        return RedirectToAction("IndexLogin", "Login");
                    }
                    return RedirectToAction("IndexLogin", "Login");
                }
                else
                {
                    var userList = context.Users.Where(x => x.u_email == u.u_email).FirstOrDefault();
                    var designatiton = context.Designations.Where(x => x.i_id == u.designation_id).FirstOrDefault();
                    var enteredPassword = u.u_password;
                  
                    if (enteredPassword == userList.u_password.ToString())
                    {
                        Session["full_name"] = userList.f_name.ToString() + " " + userList.m_name.ToString() + " " + userList.l_name.ToString();
                        Session["fname"] = userList.f_name.ToString();
                        Session["email"] = u.u_email.ToString();
                        Session["uid"] = userList.u_id.ToString();
                        Session["uprivilege"] = userList.u_privilege.ToString();
                        Session["co_no1"] = userList.contact_no_1.ToString(); ;
                        Session["last_logged"] = userList.last_logged_time.ToString();
                        Session["emp_no"] = userList.slt_employee_no.ToString();
                        Session["designation"] = designatiton;
                        Session["userId"] = userList.i_id;


                        return RedirectToAction("IndexHome", "Home");
                    }
                    else
                    {
                        TempData["vaidation"] = "Invalid Login";
                        return RedirectToAction("IndexLogin", "Login");
                    }
                }
            }
            return null;
        }

        public ActionResult DoLogout()
        {
            Session.Clear();
            ViewData.Clear();
            return RedirectToAction("IndexLogin", "Login");
        }

    }
}

