﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheOrganizer003.Models;

namespace TheOrganizer003.Util
{
    public class Utils
    {

        db_ventureEntities context = new db_ventureEntities();

        public int GenerateVentureID(int empNo, string incrementValue)
        {
            var res = empNo + incrementValue;
            var result = int.Parse(res);
            return result;
        }

        public string GetIncrementValue(int refId, int empNo)
        {
            var idr = context.IdReferences.SqlQuery("select * from IdReference where ref_id=" + refId + " and ref_value like '" + empNo + "%' ");
            //select* from IdReference where ref_id = 2 and ref_value like '15751%'

            var firstValue = idr.FirstOrDefault();
            if (idr != null && firstValue != null)
            {
                int empNoSize = empNo.ToString().Length;
                string refvalue = firstValue.ref_value.ToString();
                //int refvalueSize = firstValue.ref_value.ToString().Length;
                //int incrementValueSize = refvalueSize - empNoSize;
                string incrementVal = refvalue.Substring(empNoSize);
                return incrementVal;
            }
            else
            {
                var desc = "";
                if (refId == 1)
                {
                    desc = "Venture";
                }
                else if (refId == 2)
                {
                    desc = "User";
                }
                else if (refId == 3)
                {
                    desc = "Follower";
                }

                var refValue = empNo + "00000";
                var isAdded = context.IdReferences.SqlQuery("insert into IdReference(ref_id,ref_value,ref_description) values(" + refId + "," + int.Parse(refValue) + ",'" + desc + "');");
                context.SaveChanges();
                return "00000";
            }
        }

        public void UpdateIdReference(int refId, int empNo, string incrementNo)
        {
            int increValue = int.Parse(incrementNo) + 1;
            context.IdReferences.SqlQuery("update IdReference set ref_value =" + increValue + "  where ref_id = " + refId + " and ref_value like '" + empNo + "%'");
            context.SaveChanges();
        }

        public void addALog (string venture_id, int priority,string userid ,string description)
        {
            string logId = "lg" + venture_id;
            var cur_dt = DateTime.Now.ToString();
            //var userId = Session["uid"];

            int row_insert = context.Database.ExecuteSqlCommand("insert into Log values('" + logId + "','" + venture_id + "','" + priority + "','" + cur_dt + "','"+userid+"','" + description + "',1);");          
        }

    }
}